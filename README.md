Virual file system
----------------------------------------------------------------------------------------------------------------

Supported files: 

- virtual text files.
----------------------------------------------------------------------------------------------------------------
Supported commands:

- cd < address> (change directory)

- create_file <file_name> (creates a file)

- mkdir <folder_name> (creates a folder)

- write <file_name> <number_of_line> <text> <-override to override line> (adds to or overrides a line in file)

- cat <file_name> (shows file contents)

- ls (shows folder contents)

- wc <file_name> | <text> (finds word count in file or in text)

- exit (closes terminal)
----------------------------------------------------------------------------------------------------------------