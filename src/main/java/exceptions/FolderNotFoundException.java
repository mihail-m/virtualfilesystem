package exceptions;

public class FolderNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;
    
    public FolderNotFoundException() {
        super();
    }
    
    public FolderNotFoundException(String message) {
        super(message);
    }
}
