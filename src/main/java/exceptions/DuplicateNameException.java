package exceptions;

public class DuplicateNameException extends Exception {

    private static final long serialVersionUID = 1L;
    
    public DuplicateNameException() {
        super();
    }
    
    public DuplicateNameException(String message) {
        super(message);
    }
}
