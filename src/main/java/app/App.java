package app;

import exceptions.DuplicateNameException;
import exceptions.FileNotFoundException;
import exceptions.FolderNotFoundException;

import java.util.Scanner;

public class App {
    
    private static Scanner input;
    
    @SuppressWarnings("finally")
    private static void executeWrite(Terminal terminal, String[] words) {
        boolean override = false;
        if (words.length == 5 && words[4].equals("-override")) {
            override = true;
        }

        try {
            terminal.write(words[1], Integer.parseInt(words[2]), words[3], override);
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } finally {
            return;
        }
    }
    
    private static void executeLs(Terminal terminal, String[] words) {
        if (words.length == 1) {
            terminal.ls(false);
        } else if (words.length == 2 && words[1].equals("--sorted")) {
            terminal.ls(true);
        } else {
            System.out.println("Invalid command.");
        }
        return;
    }
    
    @SuppressWarnings("finally")
    private static void executeRemove(Terminal terminal, String[] words) {
        if (words.length == 4) {
            try {
                terminal.remove(words[1], Integer.parseInt(words[2]), 
                                Integer.parseInt(words[3]));
            } catch (FileNotFoundException e) {
                System.out.println(e.getMessage());
            } finally {
                return;
            }
        }
    }
    
    @SuppressWarnings("finally")
    private static void executeMkdir(Terminal terminal, String[] words) {
        try {
            terminal.mkdir(words[1]);
        } catch (DuplicateNameException e) {
            System.out.println(e.getMessage());
        } finally {
            return;
        }
    }
    
    @SuppressWarnings("finally")
    private static void executeCat(Terminal terminal, String[] words) {
        try {
            terminal.cat(words[1]);
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } finally {
            return;
        }
    }
    
    @SuppressWarnings("finally")
    private static void executeCreateFile(Terminal terminal, String[] words) {
        try {
            terminal.newFile(words[1]);
        } catch (DuplicateNameException e) {
            System.out.println(e.getMessage());
        } finally {
            return;
        }
    }
    
    @SuppressWarnings("finally")
    private static void executeWc(Terminal terminal, String[] words) {
        if (words.length == 2) {
            if (words[1].equals("-text")) {
                System.out.println("Terminating symbol: $end$");
                StringBuilder text = new StringBuilder("");
                
                StringBuilder lastWord = new StringBuilder("");
                do {
                    text.append(" " + input.nextLine());
                    
                    int len = text.length();
                    if (len < 5) {
                        continue;
                    }
                    lastWord = new StringBuilder("");
                    lastWord.append(text.charAt(len - 5));
                    lastWord.append(text.charAt(len - 4));
                    lastWord.append(text.charAt(len - 3));
                    lastWord.append(text.charAt(len - 2));
                    lastWord.append(text.charAt(len - 1));
                } while (lastWord.toString().equals("$end$") == false);

                System.out.println(terminal.wordCountText(text.toString()));
                return;
            } else {
                try {
                    System.out.println(terminal.wordCount(words[1]));
                } catch (FileNotFoundException e) {
                    System.out.println(e.getMessage());
                } finally {
                    return;
                }
            }
        }
    }
    
    private static void executeCd(Terminal terminal, String[] words) {
        String[] path = words[1].split("/");
        for (int i = 0; i < path.length; i++) {
            try {
                terminal.cd(path[i]);
            } catch (FolderNotFoundException e) {
                System.out.println(e.getMessage());
                for (int j = i; j >= 0; j--) {
                    try {
                        terminal.cd("..");
                    } catch (FolderNotFoundException ex) {
                        // Should never happen
                        // If it happens there is a bug in the code
                        throw new RuntimeException();
                    }
                }
                return;
            }
        }
        return;
    }   
    
    private static void execute(Terminal terminal, String command) {
        String[] words = command.split(" ");

        if (words[0].equals("exit")) {
            return;
        }

        if (words.length > 5) {
            System.out.println("Invalid command.");
            return;
        }

        if (words[0].equals("write")) {
            executeWrite(terminal, words);
            return;
        }

        if (words[0].equals("ls")) {
            executeLs(terminal, words);
            return;
        }
        
        if (words[0].equals("wc")) {
            executeWc(terminal, words);
            return;
        }
        
        if (words[0].equals("remove")) {
            executeRemove(terminal, words);
            return;
        }

        if (words.length != 2) {
            System.out.println("Invalid command.");
            return;
        }

        if (words[0].equals("cd")) {
            executeCd(terminal, words);
            return;
        }

        if (words[0].equals("mkdir")) {
            executeMkdir(terminal, words);
            return;
        }

        if (words[0].equals("create_file")) {
            executeCreateFile(terminal, words);
            return;
        }

        if (words[0].equals("cat")) {
            executeCat(terminal, words);
            return;
        }

        System.out.println("Invalid command.");
    }

    public static void main(String[] args) {
        Terminal terminal = new Terminal();

        input = new Scanner(System.in);
        String command;
        do {
            System.out.print("<" + terminal.getAdress() + ">");
            command = input.nextLine();
            execute(terminal, command);
        } while (command.equals("exit") == false);

        input.close();
    }
}
