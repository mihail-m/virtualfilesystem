package app;

import exceptions.DuplicateNameException;
import exceptions.FileNotFoundException;
import exceptions.FolderNotFoundException;
import resourses.Folder;

public class Terminal {
    private Folder current;

    public Terminal() {
        Folder base = new Folder("", "/", null);
        try {
            base.createFolder("home");
        } catch (DuplicateNameException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException();
        }

        try {
            current = base.getFolder("home");
        } catch (FolderNotFoundException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException();
        }
    }

    public String getAdress() {
        return current.getAdress();
    }

    public void cd(String name) throws FolderNotFoundException {
        if (name.equals("..")) {
            if (current.getParrent() == null) {
                return;
            }
            current = current.getParrent();
            return;
        }
        current = current.getFolder(name);
    }

    public void mkdir(String name) throws DuplicateNameException {
        current.createFolder(name);
    }

    public void newFile(String name) throws DuplicateNameException {
        current.createFile(name);
    }

    public void cat(String name) throws FileNotFoundException {
        current.getFile(name).displayContent();
    }

    public void write(String name, int index, String text, boolean override) 
            throws FileNotFoundException {
        current.writeToFile(name, index, text, override);
    }

    public void ls(boolean sorted) {
        if (sorted) {
            current.displaySortedContent();
        } else {
            current.displayContent();
        }
    }
    
    public void remove(String name, int from, int to) throws FileNotFoundException {
        for (int i = from; i <= to; i++) {
            current.getFile(name).writeLine(i, "", true);
        }
    }
    
    public int wordCountText(String text) {
        return (text.split(" ").length - 2);
    }
    
    public int wordCount(String name) throws FileNotFoundException {
        return current.getFile(name).getFileWordCount();
    }
}
