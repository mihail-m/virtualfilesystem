package resourses;

import exceptions.DuplicateNameException;
import exceptions.FileNotFoundException;
import exceptions.FolderNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Folder extends Item {
    private Folder parent;
    private HashMap<String, Folder> folders;
    private TreeMap<String, File> files;

    private void updateSize(Folder node, int delta) {
        if (node.parent == null) {
            return;
        }
        node.size += delta;
        updateSize(node.parent, delta);
    }

    public Folder(String name, String adress, Folder parrent) {
        super(name, adress);

        this.parent = parrent;
        this.folders = new HashMap<String, Folder>();
        this.files = new TreeMap<String, File>();
    }

    public void createFile(String name) throws DuplicateNameException {
        if (name == null) {
            throw new IllegalArgumentException("Illegal name in createFile method.");
        }
        
        if (files.containsKey(name)) {
            throw new DuplicateNameException("Dublicate file name");
        }

        File newFile = new File(name, this.adress + "/" + this.name);
        files.put(name, newFile);
    }

    public void writeToFile(String name, int index, String text, boolean override) 
            throws FileNotFoundException {
        if (files.get(name) == null) {
            throw new FileNotFoundException("No file with that name.");
        }
        
        int oldSize = files.get(name).getSize();
        int newSize = files.get(name).writeLine(index, text, override);

        updateSize(this, newSize - oldSize);
    }

    public void createFolder(String name) throws DuplicateNameException  {
        if (folders.get(name) != null) {
            throw new DuplicateNameException("Dublicate folder name");
        }
        
        Folder newFolder = new Folder(name, this.adress + "/" + name, this);
        folders.put(name, newFolder);
    }

    public void displayContent() {
        for (Map.Entry<String, Folder> entry : folders.entrySet()) {
            System.out.println(entry.getValue());
        }

        for (Map.Entry<String, File> entry : files.entrySet()) {
            System.out.println(entry.getValue());
        }
    }

    public void displaySortedContent() {
        ArrayList<Item> sortedList = new ArrayList<Item>();
        for (Map.Entry<String, Folder> entry : folders.entrySet()) {
            sortedList.add((Item) entry);
        }
        for (Map.Entry<String, File> entry : files.entrySet()) {
            sortedList.add((Item) entry);
        }
        sortedList.sort(new ItemComparator());
        for (int index = 0; index < sortedList.size(); index++) {
            System.out.println(sortedList.get(index));
        }
    }

    @Override
    public String toString() {
        return name + "/ " + "[size=" + size + "]";
    }

    public Folder getParrent() {
        return this.parent;
    }

    public File getFile(String name) throws FileNotFoundException {
        if (files.get(name) == null) {
            throw new FileNotFoundException("No file with that name.");
        }
        
        return files.get(name);
    }

    public Folder getFolder(String name) throws FolderNotFoundException {
        if (folders.get(name) == null) {
            throw new FolderNotFoundException("No folder with that name.");
        }
        
        
        return folders.get(name);
    }
}
