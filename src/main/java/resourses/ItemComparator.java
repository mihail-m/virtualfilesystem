package resourses;

import java.util.Comparator;

class ItemComparator implements Comparator<Item> {
    public int compare(Item o1, Item o2) {
        return (o1.size - o2.size);
    }
}