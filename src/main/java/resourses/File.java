package resourses;

import java.util.HashMap;

public class File extends Item {
    private int wordCount;
    private int lastLine;
    private HashMap<Integer, StringBuilder> line;

    public File(String name, String adress) {
        super(name, adress);

        this.lastLine = -1;
        this.line = new HashMap<Integer, StringBuilder>();
    }

    public String getLine(int index) {
        if (index < 0 || index > lastLine) {
            throw new IllegalArgumentException("Illegal index in File getLine method.");
        }

        return line.get(index).toString();
    }

    public int writeLine(int index, String text, boolean override) {
        if (index < 0 || index > lastLine && override == true) {
            throw new IllegalArgumentException("Illegal index in File writeLine method.");
        }
        
        if (text == null) {
            throw new IllegalArgumentException("Illegal text in File writeLine method.");
        }
        
        if (line.get(index) == null) {
            line.put(index, new StringBuilder(text));
        } else {
            if (override) {
                size -= line.get(index).length() * 2;
                wordCount -= line.get(index).toString().split(" ").length;
                if (text.length() == 0) {
                    line.remove(index);
                } else {
                    line.get(index).replace(0, line.get(index).length(), text);
                }
            } else {
                line.get(index).append(text);
            }
        }
        size += text.length() * 2;
        wordCount += text.split(" ").length;
        lastLine = Math.max(lastLine, index);
        return size;
    }

    public String getContent() {
        StringBuilder content = new StringBuilder("");
        for (int i = 0; i <= lastLine; i++) {
            if (line.get(i) != null) {
                if (i != lastLine) {
                    content.append(line.get(i).toString() + "\n");
                } else {
                    content.append(line.get(i).toString());
                }
            } else {
                content.append("\n");
            }
        }
        return content.toString();
    }

    public void displayContent() {
        System.out.println(getContent());
    }

    public int getFileWordCount() {
        return wordCount;
    }
}
