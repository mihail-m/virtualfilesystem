package resourses;

public class Item {
    protected String name;
    protected String adress;

    protected int size;
    
    public Item(String name, String adress) {
        if (name == null) {
            throw new IllegalArgumentException("Illegal name in Item constructor.");
        }
        
        if (adress == null) {
            throw new IllegalArgumentException("Illegal adress in Item constructor.");
        }
        
        this.adress = adress;
        this.name = name;
        this.size = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null || name.length() == 0) {
            throw new IllegalArgumentException("Illegal name in Item setName method.");
        }
        
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        if (adress == null || adress.length() == 0) {
            throw new IllegalArgumentException("Illegal adress in Item constructor.");
        }
        
        this.adress = adress;
    }
    
    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        return name + " [size=" + size + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((adress == null) ? 0 : adress.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        
        Item other = (Item) obj;
        if (adress == null && other.adress != null) {
            return false;
        }
        if (name == null && other.name != null) {
            return false;
        }

        return (size == other.size && name.equals(other.name) && adress.equals(other.adress));
    }
}
