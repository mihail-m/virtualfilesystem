package resourses;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FileTest {

    // -----------------------------------------------------------------------------
    // Test the writeLine method

    @Test
    public void writeStringWhenWriteLineIsInvokedOnANewLineReturnFileSize() {
        File newFile = new File("f", "/");
        assertEquals(8, newFile.writeLine(0, "asdf", false));
    }

    @Test
    public void writeStringWhenWriteLineIsInvokedWhitOverrideReturnFileSize() {
        File newFile = new File("f", "/");
        newFile.writeLine(0, "asdd", false);
        assertEquals(6, newFile.writeLine(0, "asd", true));
    }

    @Test
    public void writeStringWhenWriteLineIsInvokedWhitoutOverrideReturnFileSize() {
        File newFile = new File("f", "/");
        newFile.writeLine(0, "asdd", false);
        assertEquals(14, newFile.writeLine(0, "asd", false));
    }

    @Test
    public void writeStringWhenWriteLineIsInvokedOnLine50000000ReturnFileSize() {
        File newFile = new File("f", "/");
        assertEquals(8, newFile.writeLine(50000000, "asdf", false));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void negativeIndexWhenWriteLineIsInvokedThrowException() {
        File newFile = new File("f", "/");
        assertEquals("ab", newFile.writeLine(-5, "asdf", false));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void nullTextWhenWriteLineIsInvokedThrowException() {
        File newFile = new File("f", "/");
        assertEquals("ab", newFile.writeLine(5, null, false));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void indexBiggerThanLastLineWhenWriteLineIsInvokedWhitOverrideThrowException() {
        File newFile = new File("f", "/");
        newFile.writeLine(55, "abcd", false);
        assertEquals("ab", newFile.writeLine(77, "abc", true));
    }

    // -----------------------------------------------------------------------------
    // Test the getContent method
    
    @Test
    public void writeStringWhenGetContentIsInvokedOnANewLineReturnFileSize() {
        File newFile = new File("f", "/");
        newFile.writeLine(0, "asdf", false);
        assertEquals("asdf", newFile.getContent());
    }

    @Test
    public void writeStringWhenGetContenIsInvokedWhitOverrideReturnFileSize() {
        File newFile = new File("f", "/");
        newFile.writeLine(0, "asdd", false);
        newFile.writeLine(0, "asd", true);
        assertEquals("asd", newFile.getContent());
    }

    @Test
    public void writeStringWhenGetContenIsInvokedWhitoutOverrideReturnFileSize() {
        File newFile = new File("f", "/");
        newFile.writeLine(0, "asdd", false);
        newFile.writeLine(0, "asd", false);
        assertEquals("asddasd", newFile.getContent());
    }

    @Test
    public void writeStringWhenGetContenIsInvokedOnLine5ReturnFileSize() {
        File newFile = new File("f", "/");
        newFile.writeLine(5, "asdf", false);
        assertEquals("\n\n\n\n\nasdf", newFile.getContent());
    }

    // -----------------------------------------------------------------------------
    // Test the getLine method
    
    @Test
    public void indexWhenGetLineIsInvokedReturnTheTextAtLine6() {
        File newFile = new File("f", "/");
        newFile.writeLine(6, "abcd", false);
        assertEquals("abcd", newFile.getLine(6));
    }
    
    @Test
    public void indexWhenGetLineIsInvokedOnOverridenLineReturnTheTextAtLine55() {
        File newFile = new File("f", "/");
        newFile.writeLine(55, "abcd", false);
        newFile.writeLine(55, "ab", true);
        assertEquals("ab", newFile.getLine(55));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void negativeIndexWhenGetLineIsInvokedThrowException() {
        File newFile = new File("f", "/");
        newFile.writeLine(55, "abcd", false);
        assertEquals("ab", newFile.getLine(-5));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void indexWhenGetLineIsInvokedWithEmptyFileThrowException() {
        File newFile = new File("f", "/");
        assertEquals("ab", newFile.getLine(-1));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void indexBiggerThanLastLineWhenGetLineIsInvokedThrowException() {
        File newFile = new File("f", "/");
        newFile.writeLine(55, "abcd", false);
        assertEquals("ab", newFile.getLine(77));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void indexBiggerThanLastLineWhenGetLineIsInvokedWithEmptyFileThrowException() {
        File newFile = new File("f", "/");
        assertEquals("ab", newFile.getLine(77));
    }
}
